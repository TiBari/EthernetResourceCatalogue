﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EthernetResCatalogue
{
    public partial class VisitHistory : Form
    {
        public MainForm parentMain;
        public VisitHistory()
        {
            InitializeComponent();
        }

        public void DrawHistory()
        {
            if (parentMain == null) return;
            listViewHistory.Items.Clear();
            foreach (string item in parentMain.lHistory)
            {
                listViewHistory.Items.Add(new ListViewItem(new string[] { item }));
            }

            listViewHistory.Refresh();
        }

        private void VisitHistory_Load(object sender, EventArgs e)
        {
            DrawHistory();
        }
    }
}
