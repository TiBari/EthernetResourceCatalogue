﻿namespace EthernetResCatalogue
{
    partial class CatalogueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewCatalogue = new System.Windows.Forms.ListView();
            this.colResourceName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colResourceURL = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colResourceTags = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colResourceShortDesc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colResourceLastUpdate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colResourceContact = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDeleteSelected = new System.Windows.Forms.Button();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listViewCatalogue
            // 
            this.listViewCatalogue.BackColor = System.Drawing.SystemColors.Window;
            this.listViewCatalogue.CheckBoxes = true;
            this.listViewCatalogue.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colResourceName,
            this.colResourceURL,
            this.colResourceTags,
            this.colResourceShortDesc,
            this.colResourceLastUpdate,
            this.colResourceContact});
            this.listViewCatalogue.ForeColor = System.Drawing.Color.Black;
            this.listViewCatalogue.GridLines = true;
            this.listViewCatalogue.Location = new System.Drawing.Point(0, 0);
            this.listViewCatalogue.MultiSelect = false;
            this.listViewCatalogue.Name = "listViewCatalogue";
            this.listViewCatalogue.ShowGroups = false;
            this.listViewCatalogue.Size = new System.Drawing.Size(826, 308);
            this.listViewCatalogue.TabIndex = 0;
            this.listViewCatalogue.UseCompatibleStateImageBehavior = false;
            this.listViewCatalogue.View = System.Windows.Forms.View.Details;
            this.listViewCatalogue.SelectedIndexChanged += new System.EventHandler(this.listViewCatalogue_SelectedIndexChanged);
            // 
            // colResourceName
            // 
            this.colResourceName.Text = "Resource Name";
            this.colResourceName.Width = 120;
            // 
            // colResourceURL
            // 
            this.colResourceURL.Text = "URL";
            this.colResourceURL.Width = 180;
            // 
            // colResourceTags
            // 
            this.colResourceTags.Text = "Tags";
            this.colResourceTags.Width = 120;
            // 
            // colResourceShortDesc
            // 
            this.colResourceShortDesc.Text = "Short description";
            this.colResourceShortDesc.Width = 180;
            // 
            // colResourceLastUpdate
            // 
            this.colResourceLastUpdate.Text = "Last update";
            this.colResourceLastUpdate.Width = 100;
            // 
            // colResourceContact
            // 
            this.colResourceContact.Text = "Contact";
            this.colResourceContact.Width = 120;
            // 
            // btnDeleteSelected
            // 
            this.btnDeleteSelected.Location = new System.Drawing.Point(426, 314);
            this.btnDeleteSelected.Name = "btnDeleteSelected";
            this.btnDeleteSelected.Size = new System.Drawing.Size(389, 51);
            this.btnDeleteSelected.TabIndex = 1;
            this.btnDeleteSelected.Text = "Delete selected";
            this.btnDeleteSelected.UseVisualStyleBackColor = true;
            this.btnDeleteSelected.Click += new System.EventHandler(this.btnDeleteSelected_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Location = new System.Drawing.Point(13, 315);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(407, 50);
            this.btnAddNew.TabIndex = 2;
            this.btnAddNew.Text = "Add new";
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // CatalogueForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(827, 377);
            this.Controls.Add(this.btnAddNew);
            this.Controls.Add(this.btnDeleteSelected);
            this.Controls.Add(this.listViewCatalogue);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CatalogueForm";
            this.Text = "Catalogue list";
            this.Load += new System.EventHandler(this.CatalogueForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listViewCatalogue;
        private System.Windows.Forms.ColumnHeader colResourceName;
        private System.Windows.Forms.ColumnHeader colResourceURL;
        private System.Windows.Forms.ColumnHeader colResourceTags;
        private System.Windows.Forms.ColumnHeader colResourceShortDesc;
        private System.Windows.Forms.ColumnHeader colResourceLastUpdate;
        private System.Windows.Forms.ColumnHeader colResourceContact;
        private System.Windows.Forms.Button btnDeleteSelected;
        private System.Windows.Forms.Button btnAddNew;
    }
}