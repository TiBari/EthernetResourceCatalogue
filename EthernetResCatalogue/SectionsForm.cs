﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace EthernetResCatalogue
{
    public partial class SectionsForm : Form
    {
        public SectionsForm()
        {
            InitializeComponent();
            //if (Program.SectionList.Count > 0)
            //{
            //    for (int i = 0; i < Program.SectionList.Count; ++i)
            //    {
            //        listViewSections.Items.Add(new System.Windows.Forms.ListViewItem(new string[]
            //        {
            //            Program.SectionList.getItem(i).SectionName,
            //            "something"
            //        }, -1));
            //    }
            //}
        }

        private void listViewSections_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (listViewSections.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    contextMenuStrip1.Show(Cursor.Position);
                }
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            if (newSectionNameTextbox.TextLength == 0)
                btnAddNew.Enabled = false;
            else
            {
                // listViewSections.
                cSection newsection = new cSection(newSectionNameTextbox.Text);
                Program.SectionList.Add(newsection);
                listViewSections.Items.Add(new System.Windows.Forms.ListViewItem(new string[]
                {
                    newSectionNameTextbox.Text, "something"
                }, -1));
                Refresh();
            }
        }

        private void newSectionNameTextbox_TextChanged(object sender, EventArgs e)
        {
            if (newSectionNameTextbox.TextLength > 0)
                btnAddNew.Enabled = true;
            else
                btnAddNew.Enabled = false;
        }

        private void listViewSections_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            
        }
    }
}
