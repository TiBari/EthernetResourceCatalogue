﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EthernetResCatalogue
{
    class cSectionList<T> : IEnumerable
    {
        private List<T> lSections;
        private HashSet<string> hPassedSections;

        public cSectionList()
        {
            lSections = new List<T>();
            hPassedSections = new HashSet<string>();
        }

        public virtual string getName(T item)
        {
            Type tType = item.GetType();
            if (tType == typeof(cSection))
            {
                cSection nt = item as cSection;
                return nt.SectionName;
            }
            if (tType == typeof(Resource))
            {
                Resource rs = item as Resource;
                return rs.CatId.SectionName;
            }

            return null;
        }

        public int Add(T item)
        {
            lSections.Add(item);
            string name = getName(item);
            if (name != null && !hPassedSections.Add(name))
                return -1;
            return lSections.LastIndexOf(item);
        }

        public int Count
        {
            get
            {
                return lSections.Count;
            }
        }

        public bool itemExists(T item)
        {
            string name = getName(item);
            if (name == null) return false;
            return hPassedSections.Contains(name);
        }

        public T getItemByName(string name)
        {
            foreach (T val in lSections)
            {
                if (getName(val).Equals(name))
                    return val;
            }

            return default(T);
        }
        

        public void deleteItem(T item)
        {
            lSections.Remove(item);
        }

        public override int GetHashCode()
        {
            return lSections.GetHashCode() + 2;
        }

        public void Clear()
        {
            lSections.Clear();
            hPassedSections.Clear();
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)lSections).GetEnumerator();
        }
    }
}
