﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EthernetResCatalogue
{
    [Serializable]
    class Resource : IResource
    {
        public cSection CatId
        {
            get; set;
        }

        public string Contact
        {
            get; set;
        }

        public List<string> keyWords
        {
            get; set;
        }

        public DateTime lastUpdateTime
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string shortDesc
        {
            get; set;
        }

        public Uri URL
        {
            get; set;
        }

        public Resource(string Name, string URL, cSection CatId, List<string> keyWords, string shortDesc, DateTime lastUpdateTime, string Contact)
        {
            this.Name = Name;
            this.URL = new Uri(URL);
            this.CatId = CatId;
            this.keyWords = keyWords;
            this.shortDesc = shortDesc;
            this.lastUpdateTime = lastUpdateTime;
            this.Contact = Contact;

            this.CatId.AddResource(this);
        }

        public override int GetHashCode()
        {
            return CatId.GetHashCode() + Name.GetHashCode() + lastUpdateTime.GetHashCode() + keyWords.GetHashCode() + shortDesc.GetHashCode() + Contact.GetHashCode() + URL.GetHashCode();
        }
    }
}
