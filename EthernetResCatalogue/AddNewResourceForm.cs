﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EthernetResCatalogue
{
    public partial class AddNewResourceForm : Form
    {
        private MainForm mainform;
        private CatalogueForm catform;
        public AddNewResourceForm(MainForm parentmain, CatalogueForm parentform)
        {
            InitializeComponent();
            mainform = parentmain;
            catform = parentform;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                // Parse name textbox
                if (textBoxResourceName.TextLength == 0 || textBox_url.TextLength == 0 || textBox_keywords.TextLength == 0 || textBox_section.TextLength == 0)
                    throw new Exception("Not all fields can be empty, empty can be only Description and Contact fields");

                string name = textBoxResourceName.Text;
                if (Program.ResourceList.Count(y => y.Name == name) > 0)
                    throw new Exception("Item with name " + name + " already exist");
                // Parse URL textbox
                string url = textBox_url.Text;
                if (Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute) == false)
                    throw new UriFormatException("bad format for url in node `" + name + "`");

                // Parse Description textbox
                string description = textBox_desc.Text;
                if (string.IsNullOrEmpty(description))
                    description = "";

                // Parse section textbox
                string section = textBox_section.Text;
                bool isCsNew = false;
                cSection cs = Program.SectionList.getItemByName(section);
                if (cs == null)
                {
                    isCsNew = true;
                    cs = new cSection(section);
                }

                // Parse keywords textbox
                List<string> keyWords = textBox_keywords.Text.Split(',').ToList();
                if (keyWords.Count == 0)
                    throw new Exception("keywords cannot be empty on item `" + name + "`");

                // Get current DateTime
                DateTime date = DateTime.Now;

                // Parse contact string
                string contact = textBox_contact.Text;
                if (string.IsNullOrWhiteSpace(contact))
                    contact = "";

                Resource rs = new Resource(name, url, cs, keyWords, description, date, contact);
                Program.ResourceList.Add(rs);
                if (isCsNew)
                    Program.SectionList.Add(cs);
                catform.redrawCatalogueForm();
                mainform.redrawResourceList();
                Dispose();
            }
            catch (ArgumentNullException except)
            {
                MessageBox.Show(except.Message, "Argument Null Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (UriFormatException except)
            {
                MessageBox.Show(except.Message, "Bad URL", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (FormatException except)
            {
                MessageBox.Show(except.Message, "Format Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (Exception except)
            {
                MessageBox.Show(except.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }
    }
}
