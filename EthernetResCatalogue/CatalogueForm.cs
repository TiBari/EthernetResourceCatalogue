﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EthernetResCatalogue
{
    public partial class CatalogueForm : Form
    {
        private MainForm mainform;
        //private static CatalogueForm form;
        private AddNewResourceForm NewResourceForm; 
        public CatalogueForm(MainForm parentform)
        {
            // TODO read from bin file all resources
            InitializeComponent();
            mainform = parentform;
            //form = this;
            listViewCatalogue.ListViewItemSorter = new Sorter();
            listViewCatalogue.ColumnClick += listViewCatalogue_OnColumnClick;
        }

        private void listViewCatalogue_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewCatalogue.SelectedItems.Count == 0) return;
            // Get list view info to show url
            ListViewItem item = listViewCatalogue.SelectedItems[0];
            if (mainform != null)
            {
                mainform.updateWebPage(item.SubItems[0].Text, item.SubItems[1].Text);
            }
        }

        private void listViewCatalogue_OnColumnClick(object sender, ColumnClickEventArgs e)
        {
            Sorter s = (Sorter)listViewCatalogue.ListViewItemSorter;
            s.Column = e.Column;

            if (s.Order == SortOrder.Ascending)
                s.Order = SortOrder.Descending;
            else
                s.Order = SortOrder.Ascending;
            listViewCatalogue.Sort();
        }

        private void CatalogueForm_Load(object sender, EventArgs e)
        {
            redrawCatalogueForm();
            mainform.redrawResourceList();
        }
        //public static void sRedrawCatalogueForm()
        //{
        //    if (form != null)
        //        form.redrawCatalogueForm();
        //}
        public void redrawCatalogueForm()
        {
            listViewCatalogue.Items.Clear();
            foreach (Resource rs in Program.ResourceList)
            {
                ListViewItem item = new ListViewItem(new string[]
                {
                    rs.Name, rs.URL.ToString(), String.Join(", ", rs.keyWords.ToArray()).ToString(), rs.shortDesc, rs.lastUpdateTime.ToShortDateString(), rs.Contact
                });
                listViewCatalogue.Items.Add(item);
            }
        }

        private void btnDeleteSelected_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem eachItem in listViewCatalogue.CheckedItems)
            {
                Resource rmres = findResByUrl(eachItem.SubItems[1].Text);
                if (rmres != null)
                {
                    Program.ResourceList.Remove(rmres);
                    rmres.CatId.RemoveResource(rmres);
                    if (rmres.CatId.resCount == 0)
                    {
                        Program.SectionList.deleteItem(rmres.CatId);
                    }
                }
                
                listViewCatalogue.Items.Remove(eachItem);
            }
            // Redraw this form
            listViewCatalogue.Refresh();
            mainform.redrawResourceList();
        }

        private Resource findResByUrl(string url)
        {
            foreach (Resource rs in Program.ResourceList)
            {
                if (rs.URL.ToString() == url) return rs;
            }

            return null;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            NewResourceForm = new AddNewResourceForm(mainform, this);
            NewResourceForm.ShowDialog();
        }
    }
}
