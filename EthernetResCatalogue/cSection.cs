﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EthernetResCatalogue
{
    class cSection : ISection
    {
        private static int g_iLastIdentifier = 0;
        public int Identifier { get; set; }
        public string SectionName { get; set; }
        public int resCount {
            get
            {
                return ResourceList.Count;
            }
        }

        private cSectionList<Resource> ResourceList;
        public cSection(string SectionName)
        {
            ResourceList = new cSectionList<Resource>();
            Identifier = ++g_iLastIdentifier;
            this.SectionName = SectionName;
        }

        public void AddResource(Resource item)
        {
            if (item != null)
                ResourceList.Add(item);
        }

        public void RemoveResource(Resource item)
        {
            if (item != null)
                ResourceList.deleteItem(item);
        }
    }
}
