﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace EthernetResCatalogue
{
    partial class SectionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listViewSections = new System.Windows.Forms.ListView();
            this.colSectionName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSettings = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.newSectionNameTextbox = new System.Windows.Forms.TextBox();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewSections
            // 
            this.listViewSections.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colSectionName,
            this.colSettings});
            this.listViewSections.ContextMenuStrip = this.contextMenuStrip1;
            this.listViewSections.LabelWrap = false;
            this.listViewSections.Location = new System.Drawing.Point(12, 12);
            this.listViewSections.MultiSelect = false;
            this.listViewSections.Name = "listViewSections";
            this.listViewSections.ShowItemToolTips = true;
            this.listViewSections.Size = new System.Drawing.Size(654, 196);
            this.listViewSections.TabIndex = 0;
            this.listViewSections.UseCompatibleStateImageBehavior = false;
            this.listViewSections.View = System.Windows.Forms.View.Details;
            this.listViewSections.SelectedIndexChanged += new System.EventHandler(this.listViewSections_SelectedIndexChanged);
            // 
            // colSectionName
            // 
            this.colSectionName.Text = "Section Name";
            this.colSectionName.Width = 470;
            // 
            // colSettings
            // 
            this.colSettings.Text = "Settings";
            this.colSettings.Width = 179;
            // 
            // newSectionNameTextbox
            // 
            this.newSectionNameTextbox.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.newSectionNameTextbox.Location = new System.Drawing.Point(13, 229);
            this.newSectionNameTextbox.MaxLength = 64;
            this.newSectionNameTextbox.Name = "newSectionNameTextbox";
            this.newSectionNameTextbox.Size = new System.Drawing.Size(130, 20);
            this.newSectionNameTextbox.TabIndex = 1;
            this.newSectionNameTextbox.TextChanged += new System.EventHandler(this.newSectionNameTextbox_TextChanged);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Enabled = false;
            this.btnAddNew.Location = new System.Drawing.Point(166, 227);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnAddNew.TabIndex = 2;
            this.btnAddNew.Text = "Add";
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_Delete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(83, 26);
            this.contextMenuStrip1.Text = "Options";
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // toolStripMenuItem_Delete
            // 
            this.toolStripMenuItem_Delete.Name = "toolStripMenuItem_Delete";
            this.toolStripMenuItem_Delete.Size = new System.Drawing.Size(82, 22);
            this.toolStripMenuItem_Delete.Text = "Delete";
            // 
            // SectionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 261);
            this.Controls.Add(this.btnAddNew);
            this.Controls.Add(this.newSectionNameTextbox);
            this.Controls.Add(this.listViewSections);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SectionsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "SectionsForm";
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewSections;
        private System.Windows.Forms.ColumnHeader colSectionName;
        private System.Windows.Forms.ColumnHeader colSettings;
        private System.Windows.Forms.TextBox newSectionNameTextbox;
        private System.Windows.Forms.Button btnAddNew;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem toolStripMenuItem_Delete;
    }
}