﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EthernetResCatalogue
{
    interface IResource
    {
        string Name { get; set; }
        Uri URL { get; set; }
        cSection CatId { get; set; }

        List<string> keyWords { get; set; }
        string shortDesc { get; set; }
        DateTime lastUpdateTime { get; set; }
        string Contact { get; set; }
    }
}
