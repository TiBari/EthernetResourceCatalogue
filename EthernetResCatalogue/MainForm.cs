﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace EthernetResCatalogue
{
    public partial class MainForm : Form
    {
        public List<string> lHistory;
        private CatalogueForm CatalogForm;
        private string searchBoxText;

        private delegate void UpdateWebPage(string name, string newurl);
        public MainForm()
        {
            Program.SectionList = new cSectionList<cSection>();
            Program.ResourceList = new List<Resource>();

            lHistory = new List<string>();

            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Sizes the tabs of tabControl1.
            this.tabControlMain.ItemSize = new Size(90, 20);

            // Makes the tab width definable. 
            this.tabControlMain.SizeMode = TabSizeMode.Fixed;

            if (tabControlMain.TabCount == 0)
                tabControlMain.Hide();

            /// TODO: Fill Resources view list
            listViewResources.MouseDoubleClick += ListViewResources_MouseDoubleClick;

            
        }

        private void ListViewResources_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listViewResources.SelectedItems.Count == 0) return;
            // Get list view info to show url
            ListViewItem item = listViewResources.SelectedItems[0];
            updateWebPage(item.SubItems[0].Text, item.SubItems[1].Text);
            lHistory.Insert(0, item.SubItems[0].Text);
        }

        private void webMainWindow_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
        }
        
        public void redrawResourceList(string filterword = null)
        {
            /// Refresh resource list
            cSectionList<cSection> cSList = Program.SectionList;
            listViewResources.Groups.Clear();
            ListViewGroup lvg = null;
            Dictionary<string, ListViewGroup> dGroups = new Dictionary<string, ListViewGroup>();
            foreach (cSection cs in cSList)
            {
                try
                {
                    lvg = new ListViewGroup(cs.SectionName);
                    dGroups.Add(cs.SectionName, lvg);
                    listViewResources.Groups.Add(lvg);
                }
                catch (Exception)
                {
                    break;
                }
            }

            List<Resource> cResourceList;
            if (filterword != null)
            {
                //cResourceList = Program.ResourceList.Select(x => x).Where(t => t.keyWords.Contains(filterword)).ToList();
                string[] filterwords = Regex.Split(filterword, @"\W");
                //cResourceList = Program.ResourceList.Where(t => t.keyWords.Intersect(filterwords).Count() == filterwords.Count()).ToList();
                cResourceList = Program.ResourceList.Where(t => filterwords.Except(t.keyWords).Any() == false).ToList();
            }
            else
                cResourceList = Program.ResourceList;
            listViewResources.Items.Clear();
            foreach (Resource rs in cResourceList)
            {
                lvg = dGroups[rs.CatId.SectionName];
                if (lvg != null)
                {
                    listViewResources.Items.Add(new ListViewItem(new string[] { rs.Name, rs.URL.ToString() }, lvg));
                }
            }
            
            listViewResources.Refresh();
            refreshKeyAutocompleter();


        }

        private void refreshKeyAutocompleter()
        {
            textBoxSearch.AutoCompleteCustomSource.Clear();
            string[][] keys = Program.ResourceList.Distinct().Select(x => x.keyWords.Distinct().Select(t => t).ToArray()).ToArray();
            for (int i = 0; i < keys.GetLength(0); ++i)
            {
                string[] temp = keys[i];
                textBoxSearch.AutoCompleteCustomSource.AddRange(temp);
            }
        }

        //public static void updateStaticWebPage(string newurl)
        //{
        //    if (form != null)
        //        form.updateWebPage(newurl);
        //}

        public void updateWebPage(string name, string newurl)
        {
            if (InvokeRequired)
            {
                this.Invoke(new UpdateWebPage(updateWebPage), new object[] { newurl });
                return;
            }
            tabControlMain.Controls.Add(new BrowserTab(name, newurl));
            if (tabControlMain.TabCount > 0)
                tabControlMain.Show();
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnCatalogue_Click(object sender, EventArgs e)
        {
            CatalogForm = new CatalogueForm(this);
            CatalogForm.ShowDialog();
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            if (textBoxSearch.TextLength > 0)
                btnSearch.Enabled = true;
            else
            {
                btnSearch.Enabled = false;
                redrawResourceList();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchBoxText = textBoxSearch.Text;
            if (string.IsNullOrWhiteSpace(searchBoxText))
                redrawResourceList(null);
            else
                redrawResourceList(searchBoxText);
        }

        private void stopBrowseBtn_Click(object sender, EventArgs e)
        {
            tabControlMain.Controls.Remove(tabControlMain.SelectedTab);
            if (tabControlMain.TabCount == 0)
                tabControlMain.Hide();
           // webBrowserMain.Hide();
        }

        private void toolStripMenuItemFile_Load_Click(object sender, EventArgs e)
        {
            openFileDialog_Load.ShowDialog();
        }

        private void openFileDialog_Load_FileOk(object sender, CancelEventArgs e)
        {
            Program.ResourceList.Clear();
            cSectionList<cSection> sl = new cSectionList<cSection>();
            List<Resource> rl = new List<Resource>();
            HashSet<string> passedValues = new HashSet<string>();
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(openFileDialog_Load.FileName);
                //MessageBox.Show(openFileDialog_Load.FileName, "Debug", MessageBoxButtons.OK, MessageBoxIcon.None);
                XmlElement xRoot = xDoc.DocumentElement;

                XmlNodeList childnodes = xRoot.SelectNodes("resource");
                if (childnodes == null) throw new XmlException("Failed to read `resource` nodes");

                foreach (XmlNode node in childnodes)
                {
                    // Parse name attribute
                    string name = node.SelectSingleNode("@name").Value;
                    if (string.IsNullOrEmpty(name))
                        throw new XmlException("Empty name for item");
                    if (!passedValues.Add(name))
                        throw new XmlException("duplicate value in file");

                    // Parse URL attribute
                    string url = node.SelectSingleNode("@url").Value;
                    if (string.IsNullOrEmpty(url))
                        throw new XmlException("no url provided to item `" + name + "`");
                    if (Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute) == false)
                        throw new XmlException("bad format for url in node `" + name + "`");

                    // Parse Description attribute
                    string description = node.SelectSingleNode("@description").Value;
                    if (string.IsNullOrEmpty(description))
                        description = "";

                    // Parse section attribute
                    string section = node.SelectSingleNode("@section").Value;
                    if (string.IsNullOrEmpty(section))
                        throw new XmlException("no section provided to item `" + name + "`");
                    bool isCsNew = false;
                    cSection cs = sl.getItemByName(section);
                    if (cs == null)
                    {
                        isCsNew = true;
                        cs = new cSection(section);
                    }

                    // Parse keywords attribute
                    List<string> keyWords = node.SelectSingleNode("@keywords").Value.Split(',').ToList();
                    if (keyWords.Count == 0)
                        throw new XmlException("keywords cannot be empty on item `" + name + "`");

                    // Parse DateTime attribute
                    DateTime date = DateTime.Parse(node.SelectSingleNode("@lastupdate").Value);

                    // Parse contact string
                    string contact = node.SelectSingleNode("@contact").Value;
                    if (string.IsNullOrWhiteSpace(contact))
                        contact = "";

                    Resource rs = new Resource(name, url, cs, keyWords, description, date, contact);
                    rl.Add(rs);
                    if (isCsNew)
                        sl.Add(cs);
                }
            }
            catch (ArgumentNullException except)
            {
                MessageBox.Show(except.Message, "Argument Null Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (FormatException except)
            {
                MessageBox.Show(except.Message, "Format Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (XmlException except)
            {
                MessageBox.Show(except.Message, "XML Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (Exception)
            {
                return;
            }

            Program.SectionList = sl;
            Program.ResourceList = rl;

            redrawResourceList(null);
            if (CatalogForm != null && !CatalogForm.IsDisposed)
                CatalogForm.Dispose();
        }

        private void toolStripMenuItemFile_Save_Click(object sender, EventArgs e)
        {
            saveFileDialog_Save.ShowDialog();
        }

        private void saveFile(string name = null)
        {
            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(dec);
            XmlElement xRoot = doc.CreateElement("resources");
            doc.AppendChild(xRoot);
            foreach (Resource rs in Program.ResourceList)
            {
                XmlElement elem = doc.CreateElement("resource");
                elem.SetAttribute("name", rs.Name);
                elem.SetAttribute("description", rs.shortDesc);
                elem.SetAttribute("url", rs.URL.ToString());
                elem.SetAttribute("section", rs.CatId.SectionName);
                elem.SetAttribute("keywords", string.Join(",", rs.keyWords.ToArray()));
                elem.SetAttribute("lastupdate", rs.lastUpdateTime.ToString());
                elem.SetAttribute("contact", rs.Contact);
                xRoot.AppendChild(elem);
            }
            
            doc.Save(name);
            // Serialization disabled due to class protection.
        }

        private void toolStripMenuItemFile_New_Click(object sender, EventArgs e)
        {
            Program.SectionList.Clear();
            Program.ResourceList.Clear();
            redrawResourceList(null);

            if (CatalogForm != null && !CatalogForm.IsDisposed)
                CatalogForm.Dispose();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void toolStripButton_About_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.ShowDialog();
        }

        private void saveFileDialog_Save_FileOk(object sender, CancelEventArgs e)
        {
            saveFile(saveFileDialog_Save.FileName);
        }

        private void OnDrawItem_Tab(object sender, DrawItemEventArgs e)
        {
            //This code will render a "x" mark at the end of the Tab caption.
            e.Graphics.DrawString("x", e.Font, Brushes.Black, e.Bounds.Right - 15, e.Bounds.Top + 4);
            e.Graphics.DrawString(this.tabControlMain.TabPages[e.Index].Text, e.Font, Brushes.Black, e.Bounds.Left + 12, e.Bounds.Top + 4);
            e.DrawFocusRectangle();
        }

        private void OnMouseDown_Tab(object sender, MouseEventArgs e)
        {
            //Looping through the controls.
            for (int i = 0; i < this.tabControlMain.TabPages.Count; i++)
            {
                Rectangle r = tabControlMain.GetTabRect(i);
                //Getting the position of the "x" mark.
                Rectangle closeButton = new Rectangle(r.Right - 15, r.Top + 4, 9, 7);
                if (closeButton.Contains(e.Location))
                {
                    if (MessageBox.Show("Would you like to Close this Tab?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        this.tabControlMain.TabPages.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        private void toolStripButton_History_Click(object sender, EventArgs e)
        {
            var vh = new VisitHistory();
            vh.parentMain = this;
            vh.ShowDialog();
        }
    }

    public class BrowserTab : TabPage
    {
        WebBrowser wb = new WebBrowser();
        
        public BrowserTab()
        {
            wb.ScriptErrorsSuppressed = true;
            setBounds();
            wb.GoHome();
            this.Controls.Add(wb);
            this.Text = "Home";
        }

        public BrowserTab(string name, string url)
        {
            wb.ScriptErrorsSuppressed = true;
            setBounds();
            wb.Url = new Uri(url);
            this.Controls.Add(wb);
            this.Text = name;
        }

        private void setBounds()
        {
            wb.SetBounds(0, 0, 700, 375);
        }
    }
}
