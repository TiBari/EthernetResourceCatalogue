﻿namespace EthernetResCatalogue
{
    partial class AddNewResourceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxResourceName = new System.Windows.Forms.TextBox();
            this.label_resname = new System.Windows.Forms.Label();
            this.label_desc = new System.Windows.Forms.Label();
            this.textBox_desc = new System.Windows.Forms.TextBox();
            this.label_url = new System.Windows.Forms.Label();
            this.textBox_url = new System.Windows.Forms.TextBox();
            this.label_section = new System.Windows.Forms.Label();
            this.textBox_section = new System.Windows.Forms.TextBox();
            this.label_keywords = new System.Windows.Forms.Label();
            this.textBox_keywords = new System.Windows.Forms.TextBox();
            this.label_contact = new System.Windows.Forms.Label();
            this.textBox_contact = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxResourceName
            // 
            this.textBoxResourceName.Location = new System.Drawing.Point(123, 12);
            this.textBoxResourceName.MaxLength = 512;
            this.textBoxResourceName.Name = "textBoxResourceName";
            this.textBoxResourceName.Size = new System.Drawing.Size(381, 20);
            this.textBoxResourceName.TabIndex = 0;
            // 
            // label_resname
            // 
            this.label_resname.AutoSize = true;
            this.label_resname.Location = new System.Drawing.Point(12, 15);
            this.label_resname.Name = "label_resname";
            this.label_resname.Size = new System.Drawing.Size(82, 13);
            this.label_resname.TabIndex = 1;
            this.label_resname.Text = "Resource name";
            // 
            // label_desc
            // 
            this.label_desc.AutoSize = true;
            this.label_desc.Location = new System.Drawing.Point(12, 43);
            this.label_desc.Name = "label_desc";
            this.label_desc.Size = new System.Drawing.Size(107, 13);
            this.label_desc.TabIndex = 3;
            this.label_desc.Text = "Resource description";
            // 
            // textBox_desc
            // 
            this.textBox_desc.Location = new System.Drawing.Point(123, 40);
            this.textBox_desc.Multiline = true;
            this.textBox_desc.Name = "textBox_desc";
            this.textBox_desc.Size = new System.Drawing.Size(381, 41);
            this.textBox_desc.TabIndex = 2;
            // 
            // label_url
            // 
            this.label_url.AutoSize = true;
            this.label_url.Location = new System.Drawing.Point(12, 93);
            this.label_url.Name = "label_url";
            this.label_url.Size = new System.Drawing.Size(78, 13);
            this.label_url.TabIndex = 5;
            this.label_url.Text = "Resource URL";
            // 
            // textBox_url
            // 
            this.textBox_url.Location = new System.Drawing.Point(123, 90);
            this.textBox_url.Name = "textBox_url";
            this.textBox_url.Size = new System.Drawing.Size(381, 20);
            this.textBox_url.TabIndex = 4;
            // 
            // label_section
            // 
            this.label_section.AutoSize = true;
            this.label_section.Location = new System.Drawing.Point(12, 123);
            this.label_section.Name = "label_section";
            this.label_section.Size = new System.Drawing.Size(43, 13);
            this.label_section.TabIndex = 7;
            this.label_section.Text = "Section";
            // 
            // textBox_section
            // 
            this.textBox_section.Location = new System.Drawing.Point(123, 120);
            this.textBox_section.Name = "textBox_section";
            this.textBox_section.Size = new System.Drawing.Size(381, 20);
            this.textBox_section.TabIndex = 6;
            // 
            // label_keywords
            // 
            this.label_keywords.AutoSize = true;
            this.label_keywords.Location = new System.Drawing.Point(12, 149);
            this.label_keywords.Name = "label_keywords";
            this.label_keywords.Size = new System.Drawing.Size(53, 13);
            this.label_keywords.TabIndex = 9;
            this.label_keywords.Text = "Keywords";
            // 
            // textBox_keywords
            // 
            this.textBox_keywords.Location = new System.Drawing.Point(123, 146);
            this.textBox_keywords.Name = "textBox_keywords";
            this.textBox_keywords.Size = new System.Drawing.Size(381, 20);
            this.textBox_keywords.TabIndex = 8;
            // 
            // label_contact
            // 
            this.label_contact.AutoSize = true;
            this.label_contact.Location = new System.Drawing.Point(12, 175);
            this.label_contact.Name = "label_contact";
            this.label_contact.Size = new System.Drawing.Size(44, 13);
            this.label_contact.TabIndex = 11;
            this.label_contact.Text = "Contact";
            // 
            // textBox_contact
            // 
            this.textBox_contact.Location = new System.Drawing.Point(123, 172);
            this.textBox_contact.Name = "textBox_contact";
            this.textBox_contact.Size = new System.Drawing.Size(381, 20);
            this.textBox_contact.TabIndex = 10;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(431, 226);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(350, 225);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 13;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // AddNewResourceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(518, 261);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label_contact);
            this.Controls.Add(this.textBox_contact);
            this.Controls.Add(this.label_keywords);
            this.Controls.Add(this.textBox_keywords);
            this.Controls.Add(this.label_section);
            this.Controls.Add(this.textBox_section);
            this.Controls.Add(this.label_url);
            this.Controls.Add(this.textBox_url);
            this.Controls.Add(this.label_desc);
            this.Controls.Add(this.textBox_desc);
            this.Controls.Add(this.label_resname);
            this.Controls.Add(this.textBoxResourceName);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddNewResourceForm";
            this.Text = "Add new resource";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxResourceName;
        private System.Windows.Forms.Label label_resname;
        private System.Windows.Forms.Label label_desc;
        private System.Windows.Forms.TextBox textBox_desc;
        private System.Windows.Forms.Label label_url;
        private System.Windows.Forms.TextBox textBox_url;
        private System.Windows.Forms.Label label_section;
        private System.Windows.Forms.TextBox textBox_section;
        private System.Windows.Forms.Label label_keywords;
        private System.Windows.Forms.TextBox textBox_keywords;
        private System.Windows.Forms.Label label_contact;
        private System.Windows.Forms.TextBox textBox_contact;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAdd;
    }
}