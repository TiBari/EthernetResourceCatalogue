﻿namespace EthernetResCatalogue
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("test group", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("nextonegroup", System.Windows.Forms.HorizontalAlignment.Left);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripBtnFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItemFile_New = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemFile_Load = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemFile_Save = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCatalogue = new System.Windows.Forms.ToolStripButton();
            this.stopBrowseBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_About = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listViewResources = new System.Windows.Forms.ListView();
            this.colResourceName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.labelSearch = new System.Windows.Forms.Label();
            this.openFileDialog_Load = new System.Windows.Forms.OpenFileDialog();
            this.btnSearch = new System.Windows.Forms.Button();
            this.saveFileDialog_Save = new System.Windows.Forms.SaveFileDialog();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_History = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBtnFile,
            this.toolStripSeparator1,
            this.btnCatalogue,
            this.stopBrowseBtn,
            this.toolStripSeparator3,
            this.toolStripButton_History,
            this.toolStripSeparator2,
            this.toolStripButton_About});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(974, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripBtnFile
            // 
            this.toolStripBtnFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripBtnFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemFile_New,
            this.toolStripMenuItemFile_Load,
            this.toolStripMenuItemFile_Save,
            this.exitToolStripMenuItem});
            this.toolStripBtnFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnFile.Image")));
            this.toolStripBtnFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnFile.Name = "toolStripBtnFile";
            this.toolStripBtnFile.Size = new System.Drawing.Size(38, 22);
            this.toolStripBtnFile.Text = "File";
            // 
            // toolStripMenuItemFile_New
            // 
            this.toolStripMenuItemFile_New.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripMenuItemFile_New.Name = "toolStripMenuItemFile_New";
            this.toolStripMenuItemFile_New.Size = new System.Drawing.Size(100, 22);
            this.toolStripMenuItemFile_New.Text = "New";
            this.toolStripMenuItemFile_New.Click += new System.EventHandler(this.toolStripMenuItemFile_New_Click);
            // 
            // toolStripMenuItemFile_Load
            // 
            this.toolStripMenuItemFile_Load.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripMenuItemFile_Load.Name = "toolStripMenuItemFile_Load";
            this.toolStripMenuItemFile_Load.Size = new System.Drawing.Size(100, 22);
            this.toolStripMenuItemFile_Load.Text = "Load";
            this.toolStripMenuItemFile_Load.Click += new System.EventHandler(this.toolStripMenuItemFile_Load_Click);
            // 
            // toolStripMenuItemFile_Save
            // 
            this.toolStripMenuItemFile_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripMenuItemFile_Save.Name = "toolStripMenuItemFile_Save";
            this.toolStripMenuItemFile_Save.Size = new System.Drawing.Size(100, 22);
            this.toolStripMenuItemFile_Save.Text = "Save";
            this.toolStripMenuItemFile_Save.Click += new System.EventHandler(this.toolStripMenuItemFile_Save_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnCatalogue
            // 
            this.btnCatalogue.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnCatalogue.Image = ((System.Drawing.Image)(resources.GetObject("btnCatalogue.Image")));
            this.btnCatalogue.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCatalogue.Name = "btnCatalogue";
            this.btnCatalogue.Size = new System.Drawing.Size(65, 22);
            this.btnCatalogue.Text = "Catalogue";
            this.btnCatalogue.Click += new System.EventHandler(this.btnCatalogue_Click);
            // 
            // stopBrowseBtn
            // 
            this.stopBrowseBtn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.stopBrowseBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stopBrowseBtn.Image = ((System.Drawing.Image)(resources.GetObject("stopBrowseBtn.Image")));
            this.stopBrowseBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stopBrowseBtn.Name = "stopBrowseBtn";
            this.stopBrowseBtn.Size = new System.Drawing.Size(23, 22);
            this.stopBrowseBtn.Text = "Close browser";
            this.stopBrowseBtn.Click += new System.EventHandler(this.stopBrowseBtn_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_About
            // 
            this.toolStripButton_About.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_About.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_About.Image")));
            this.toolStripButton_About.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_About.Name = "toolStripButton_About";
            this.toolStripButton_About.Size = new System.Drawing.Size(44, 22);
            this.toolStripButton_About.Text = "About";
            this.toolStripButton_About.Click += new System.EventHandler(this.toolStripButton_About_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listViewResources);
            this.panel1.Location = new System.Drawing.Point(13, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(237, 341);
            this.panel1.TabIndex = 3;
            // 
            // listViewResources
            // 
            this.listViewResources.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listViewResources.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colResourceName});
            listViewGroup3.Header = "test group";
            listViewGroup3.Name = "listViewGroup1";
            listViewGroup4.Header = "nextonegroup";
            listViewGroup4.Name = "nextonegroup";
            this.listViewResources.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup3,
            listViewGroup4});
            this.listViewResources.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewResources.Location = new System.Drawing.Point(4, 4);
            this.listViewResources.MultiSelect = false;
            this.listViewResources.Name = "listViewResources";
            this.listViewResources.Size = new System.Drawing.Size(230, 334);
            this.listViewResources.TabIndex = 0;
            this.listViewResources.UseCompatibleStateImageBehavior = false;
            this.listViewResources.View = System.Windows.Forms.View.Details;
            this.listViewResources.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // colResourceName
            // 
            this.colResourceName.Text = "Resource name";
            this.colResourceName.Width = 226;
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.AutoCompleteCustomSource.AddRange(new string[] {
            "book",
            "self",
            "pro"});
            this.textBoxSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBoxSearch.Location = new System.Drawing.Point(13, 388);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(141, 20);
            this.textBoxSearch.TabIndex = 5;
            this.textBoxSearch.TextChanged += new System.EventHandler(this.textBoxSearch_TextChanged);
            // 
            // labelSearch
            // 
            this.labelSearch.AutoSize = true;
            this.labelSearch.Location = new System.Drawing.Point(10, 373);
            this.labelSearch.Name = "labelSearch";
            this.labelSearch.Size = new System.Drawing.Size(41, 13);
            this.labelSearch.TabIndex = 6;
            this.labelSearch.Text = "Search";
            // 
            // openFileDialog_Load
            // 
            this.openFileDialog_Load.Filter = "XML Files (*.xml)|*.xml";
            this.openFileDialog_Load.Title = "Load file";
            this.openFileDialog_Load.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_Load_FileOk);
            // 
            // btnSearch
            // 
            this.btnSearch.Enabled = false;
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(160, 381);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(32, 33);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // saveFileDialog_Save
            // 
            this.saveFileDialog_Save.DefaultExt = "xml";
            this.saveFileDialog_Save.Filter = "XML files|*.xml";
            this.saveFileDialog_Save.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog_Save_FileOk);
            // 
            // tabControlMain
            // 
            this.tabControlMain.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControlMain.Location = new System.Drawing.Point(256, 33);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(706, 375);
            this.tabControlMain.TabIndex = 8;
            this.tabControlMain.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawItem_Tab);
            this.tabControlMain.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown_Tab);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_History
            // 
            this.toolStripButton_History.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_History.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_History.Image")));
            this.toolStripButton_History.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_History.Name = "toolStripButton_History";
            this.toolStripButton_History.Size = new System.Drawing.Size(49, 22);
            this.toolStripButton_History.Text = "History";
            this.toolStripButton_History.Click += new System.EventHandler(this.toolStripButton_History_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(974, 421);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.labelSearch);
            this.Controls.Add(this.textBoxSearch);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Ethernet Resources — Course Work";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView listViewResources;
        private System.Windows.Forms.ColumnHeader colResourceName;
        private System.Windows.Forms.ToolStripButton btnCatalogue;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.ToolStripButton stopBrowseBtn;
        private System.Windows.Forms.Label labelSearch;
        private System.Windows.Forms.ToolStripDropDownButton toolStripBtnFile;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFile_New;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFile_Load;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFile_Save;
        private System.Windows.Forms.OpenFileDialog openFileDialog_Load;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton_About;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.SaveFileDialog saveFileDialog_Save;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_History;
    }
}